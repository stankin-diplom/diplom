package code.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;

public interface StorageService {
    URI uploadFile(MultipartFile file) throws IOException;
}
