package code.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.UUID;

@Service
public class FileStorageService implements StorageService{

    private final String uploadPath;

    @Autowired
    public FileStorageService(@Value("${upload.path}") String uploadPath) {
        this.uploadPath = uploadPath;
    }

    @Override
    public URI uploadFile(MultipartFile file) throws IOException {
        File upFile = new File(uploadPath + "/" + UUID.randomUUID().toString() + "_" + file.getOriginalFilename());

        try {
            if (!upFile.exists()) {
                upFile.createNewFile();
            }else {
                throw new RuntimeException(String.format("File with name [%s] already exists.", file.getOriginalFilename()));
            }

            try (FileOutputStream outputStream = new FileOutputStream(upFile);){
                outputStream.write(file.getBytes());
            }
        }finally {
            file.getInputStream().close();
        }

        return upFile.toURI();
    }
}
