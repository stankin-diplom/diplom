package code.entities.user;

import code.entities.catalogue.ExpositionCatalogueEntity;
import code.entities.delegation.DelegationEntity;
import code.entities.membership.MembershipEntity;
import code.entities.organization.OrganizationEntity;
import code.entities.tech.TechnologyEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "usr")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "usr_id")
    private Long id;

    private String name;

    private String secondName;

    private String surname;

    private String phoneNumber;

    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private OrganizationEntity organization;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "users")
    private Set<TechnologyEntity> technologies = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<MembershipEntity> membership = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "creator")
    private Set<ExpositionCatalogueEntity> catalogues = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "creator")
    private Set<DelegationEntity> delegations = new HashSet<>();
}
