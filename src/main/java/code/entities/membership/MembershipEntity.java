package code.entities.membership;

import code.entities.catalogue.ExpositionCatalogueEntity;
import code.entities.delegation.DelegationEntity;
import code.entities.event.EventEntity;
import code.entities.logistic.LogisticEntity;
import code.entities.organization.OrganizationEntity;
import code.entities.tech.TechnologyEntity;
import code.entities.user.UserEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "event_membership")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(of = "id")
public class MembershipEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "memb_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id")
    private EventEntity event;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usr_id")
    private UserEntity user;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "membership")
    @Size(max = 5)
    private Set<TechnologyEntity> technologies = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deleg_id")
    private DelegationEntity delegation;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "membership")
    private LogisticEntity logistic;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cat_id")
    private ExpositionCatalogueEntity catalogue;

    @ElementCollection
    @CollectionTable(name = "multimedia", joinColumns = @JoinColumn(name = "memb_id"))
    private Set<String> multimediaLinks = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private OrganizationEntity organization;

    private String contestApplicationLink;

    private String documentLink;

    private Boolean isApproved;
}
