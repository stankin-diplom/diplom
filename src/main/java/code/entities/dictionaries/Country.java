package code.entities.dictionaries;

import code.entities.logistic.LogisticEntity;
import code.entities.organization.OrganizationAddress;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "countries")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "country_id")
    private Long id;

    @NotBlank
    @NotNull
    private String ruName;

    @NotBlank
    @NotNull
    private String engName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
    private Set<LogisticEntity> logistics = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
    private Set<OrganizationAddress> organizationAddresses = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "country")
    private Set<Subject> subjects = new HashSet<>();
}
