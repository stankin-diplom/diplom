package code.entities.dictionaries;

import code.entities.organization.OrganizationAddress;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "streets")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(of = "id")
public class Street {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "street_id")
    private Long id;

    @NotNull
    @NotBlank
    private String ruName;

    @NotNull
    @NotBlank
    private String engName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "street")
    private Set<House> houses = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "street")
    private Set<OrganizationAddress> organizationAddresses = new HashSet<>();
}
