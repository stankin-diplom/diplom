package code.entities.dictionaries;

import code.entities.organization.OrganizationAddress;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "subjects")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(of = "id")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "subject_id")
    private Long id;

    @NotNull
    @NotBlank
    private String ruName;

    @NotBlank
    @NotNull
    private String engName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "subject")
    private Set<City> cities = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "subject")
    private Set<OrganizationAddress> organizationAddresses = new HashSet<>();

}
