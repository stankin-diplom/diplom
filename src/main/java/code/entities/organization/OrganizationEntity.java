package code.entities.organization;

import code.converters.util.OrganizationFormUtil;
import code.converters.util.ParticipationFormUtil;
import code.entities.delegation.DelegationEntity;
import code.entities.membership.MembershipEntity;
import code.entities.tech.Author;
import code.entities.tech.TechnologyEntity;
import code.entities.user.UserEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "organizations")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class OrganizationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "org_id")
    private Long id;

    private OrganizationForm form;

    @Column(unique = true)
    private String ruName;

    @Column(unique = true)
    private String enName;

    private String ruLogo;

    private String enLogo;

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "organization")
    private OrganizationAddress organizationAddress;

    @Column(unique = true)
    private String phoneNumber;

    @Column(unique = true)
    private String fax;

    @Column(unique = true)
    private String mail;

    @Column(unique = true)
    private String url;

    private ParticipationForm participation;

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "organization")
    private ResponsibleEmployee responsibleEmployee;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organization", cascade = CascadeType.ALL)
    private Set<UserEntity> users = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organization")
    private Set<Author> technologiesAuthors = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "organization")
    private Set<DelegationEntity> delegations = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organization")
    private Set<TechnologyEntity> technologies = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organization")
    private Set<MembershipEntity> membership = new HashSet<>();


    @JsonProperty(value = "form")
    private void setJsonForm(String form) {
        this.form = OrganizationFormUtil.determineType(form);
    }

    @JsonProperty(value = "form")
    private String getJsonForm() {
        return form.getRussianName();
    }

    @JsonProperty(value = "participation")
    private void setJsonParticipation(String participation) {
        this.participation = ParticipationFormUtil.determineType(participation);
    }

    @JsonProperty(value = "participation")
    private String getJsonParticipation() {
        return participation.getValue();
    }
}
