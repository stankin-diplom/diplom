package code.entities.organization;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "responsible_employee")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponsibleEmployee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "employee_id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private OrganizationEntity organization;

    private String name;

    private String position;

    private String phoneNumber;

    private String email;
}
