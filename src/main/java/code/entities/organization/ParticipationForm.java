package code.entities.organization;

import lombok.Getter;

public enum ParticipationForm {
    INTERNAL("Очная"), DISTANT("Заочная");

    @Getter
    private final String value;

    ParticipationForm(String value) {
        this.value = value;
    }
}
