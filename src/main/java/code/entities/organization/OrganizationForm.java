package code.entities.organization;

import lombok.Getter;

public enum OrganizationForm {
    FGBOU_VO("ФГБОУ ВО"), FGBUN("ФГБУН"), NPO("НПО"), OOO("ООО");

    @Getter
    private String russianName;

    OrganizationForm(String russianName) {
        this.russianName = russianName;
    }
}
