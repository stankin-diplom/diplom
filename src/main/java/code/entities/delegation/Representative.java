package code.entities.delegation;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "representatives")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
public class Representative {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name,
            secondName,
            surname,
            ruPosition,
            engPosition,
            phoneNumber,
            theme;

    private Date birthDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "deleg_id")
    private Set<DelegationEntity> delegations = new HashSet<>();


}
