package code.entities.delegation;

import code.entities.membership.MembershipEntity;
import code.entities.organization.OrganizationEntity;
import code.entities.user.UserEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "delegations")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(of = "id")
public class DelegationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "deleg_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private OrganizationEntity organization;

    private Boolean isAirwayTicketsBookingNeeded;

    private Boolean isAccommodationBookingNeeded;

    private Boolean isTransferBookingNeeded;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, mappedBy = "delegations")
    private Set<Representative> representatives = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "delegation")
    private Set<MembershipEntity> membership = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usr_id")
    private UserEntity creator;
}
