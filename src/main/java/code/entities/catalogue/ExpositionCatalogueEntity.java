package code.entities.catalogue;

import code.entities.membership.MembershipEntity;
import code.entities.organization.OrganizationEntity;
import code.entities.tech.TechnologyEntity;
import code.entities.user.UserEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "expositions_catalogues")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@EqualsAndHashCode(of = "id")
public class ExpositionCatalogueEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cat_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private OrganizationEntity organization;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "tech_id")
    @Size(max = 5)
    private Set<TechnologyEntity> technologies = new HashSet<>();

    private String ruOrgDirection, engOrgDirection;

    @Size(max = 1000)
    private String ruOrgDescription, engOrgDescription;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "catalogue")
    private Set<MembershipEntity> membership = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usr_id")
    private UserEntity creator;
}
