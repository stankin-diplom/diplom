package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static code.converters.util.IntellectualRightsUtil.determineType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IntellectualPropertyRights {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tech_id")
    private TechnologyEntity technology;

    @NotNull
    private IntellectualPropertyRightsTypes type;

    @JsonProperty(value = "type")
    private void setJsonType(String type) {
        this.type = determineType(type);
    }

    @JsonProperty(value = "type")
    private String getJsonType() {
        return type.getValue();
    }

}
