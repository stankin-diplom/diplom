package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.Date;

import static code.entities.tech.IntellectualPropertyRightsTypes.EUROPEAN_PATENT;

@Entity
@NoArgsConstructor
public class EuropeanPatent extends CountryPatent {

    @Builder
    @JsonCreator
    public EuropeanPatent(@JsonProperty("id") Long id,
                          @JsonProperty("technology") TechnologyEntity technology,
                          @JsonProperty("number") String number,
                          @JsonProperty("registrationDate") Date registrationDate) {
        super(id, technology, EUROPEAN_PATENT, number, registrationDate);
    }
}
