package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnershipType.JOINT_VENTURE_AGREEMENT;

@Entity
@NoArgsConstructor
public class JointVentureAgreement extends Partnership{

    @Builder
    @JsonCreator
    public JointVentureAgreement(@JsonProperty("id") Long id,
                                 @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, JOINT_VENTURE_AGREEMENT);
    }
}
