package code.entities.tech;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static code.converters.util.PartnerUtil.determineType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Partner {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tech_id")
    private TechnologyEntity technology;

    @NotNull
    private PartnerType type;

    @JsonProperty(value = "type")
    private void setJsonType(Object object){
        this.type = determineType(object.toString());
    }

    @JsonProperty(value = "type")
    private String getJsonType(){
        return type.getValue();
    }
}
