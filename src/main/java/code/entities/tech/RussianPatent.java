package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.Date;

import static code.entities.tech.IntellectualPropertyRightsTypes.RUSSIAN_PATENT;

@Entity
@NoArgsConstructor
public class RussianPatent extends CountryPatent {

    @Builder
    @JsonCreator
    public RussianPatent(@JsonProperty("id") Long id,
                         @JsonProperty("technology") TechnologyEntity technology,
                         @JsonProperty("number") String number,
                         @JsonProperty("registrationDate") Date registrationDate) {
        super(id, technology, RUSSIAN_PATENT, number, registrationDate);
    }
}
