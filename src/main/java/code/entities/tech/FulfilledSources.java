package code.entities.tech;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
@NoArgsConstructor
public class FulfilledSources extends FinancialSources {


    private String programName, customer, contractNumber, projectName;

    public FulfilledSources(Long id,
                            String type,
                            TechnologyEntity technology,
                            String programName,
                            String customer,
                            String contractNumber,
                            String projectName) {
        super(id, type, technology);
        this.programName = programName;
        this.customer = customer;
        this.contractNumber = contractNumber;
        this.projectName = projectName;
    }
}
