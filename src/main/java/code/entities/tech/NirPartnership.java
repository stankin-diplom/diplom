package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnershipType.NIR;

@Entity
@NoArgsConstructor
public class NirPartnership extends Partnership{

    @Builder
    @JsonCreator
    public NirPartnership(@JsonProperty("id") Long id,
                          @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, NIR);
    }
}
