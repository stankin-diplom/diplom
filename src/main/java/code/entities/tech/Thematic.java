package code.entities.tech;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Thematic {
    MEDICINE("Медицина"),PRODUCTION("Производсвто"),ENERGETIC("Энергетика"),AGRICULTURE("Сельское хозяйство")
    ,SOCIETY("Общество"),SECURITY("Безопасность"),TERRITORY("Освоение территории");

    @Getter
    private String value;
}
