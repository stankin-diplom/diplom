package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnerType.PRODUCTION_ORGANIZATIONS;

@Entity
@NoArgsConstructor
public class ProductionOrganization extends Partner{

    @Builder
    @JsonCreator
    public ProductionOrganization(@JsonProperty("id") Long id,
                                  @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, PRODUCTION_ORGANIZATIONS);
    }
}
