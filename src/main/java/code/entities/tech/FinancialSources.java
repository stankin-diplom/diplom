package code.entities.tech;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinancialSources {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "fin_id")
    private Long id;

    private String type;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tech_id")
    private TechnologyEntity technology;
}
