package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnerType.BUSINESS;

@NoArgsConstructor
@Entity
public class Business extends Partner{

    @Builder
    @JsonCreator
    public Business(@JsonProperty("id") Long id,
                    @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, BUSINESS);
    }
}
