package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnershipType.FINANCIATION;

@Entity
@NoArgsConstructor
public class FinanciationPartnership extends Partnership{

    @Builder
    @JsonCreator
    public FinanciationPartnership(@JsonProperty("id") Long id,
                                   @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, FINANCIATION);
    }
}
