package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnerType.EDUCATIONAL_SUBJECTS;

@Entity
@NoArgsConstructor
public class EducationalSubjects extends Partner{

    @Builder
    @JsonCreator
    public EducationalSubjects(@JsonProperty("id") Long id,
                               @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, EDUCATIONAL_SUBJECTS);
    }
}
