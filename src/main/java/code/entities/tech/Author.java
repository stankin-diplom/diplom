package code.entities.tech;

import code.entities.organization.OrganizationEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "technology_authors")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@EqualsAndHashCode(of = "id")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String degree;

    private String title;

    private String position;

    @ManyToOne
    @JoinColumn(name = "org_id")
    private OrganizationEntity organization;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "tech_id")
    private Set<TechnologyEntity> technologies = new HashSet<>();
}
