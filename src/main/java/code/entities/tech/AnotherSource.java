package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@NoArgsConstructor
@Data
public class AnotherSource extends FinancialSources{

    private String source;

    @Builder
    @JsonCreator
    public AnotherSource(@JsonProperty("id") Long id,
                         @JsonProperty("technology") TechnologyEntity technology,
                         @JsonProperty("source") String source) {
        super(id, "Другое", technology);
        this.source = source;
    }
}
