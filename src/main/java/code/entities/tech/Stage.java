package code.entities.tech;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum  Stage {
    READY("Готовая продукция"), PRODUCTION_EXAMPLE("Промышленный образец"), TEST_EXAMLE("Опытный образец"),
    NIR("НИР"),OKR("ОКР"),DOCUMENTATION("Проектно-сметная документация"),NIOKR("Промежуточный НИОКР"),
    RESEARCH("Дополнительные исследования");

    @Getter
    private String value;
}
