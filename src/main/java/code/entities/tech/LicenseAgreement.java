package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnershipType.LICENSE_AGREEMENT;

@Entity
@NoArgsConstructor
public class LicenseAgreement extends Partnership{

    @Builder
    @JsonCreator
    public LicenseAgreement(@JsonProperty("id") Long id,
                            @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, LICENSE_AGREEMENT);
    }
}
