package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.IntellectualPropertyRightsTypes.PATENT_APPLICATION;

@Entity
@NoArgsConstructor
public class PatentApplication extends IntellectualPropertyRights{

    @Builder
    @JsonCreator
    public PatentApplication(@JsonProperty("id") Long id,
                             @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, PATENT_APPLICATION);
    }
}
