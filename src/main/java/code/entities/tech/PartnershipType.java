package code.entities.tech;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum PartnershipType {
    TECHNICAL("Техническое сотрудничество"),NIR("НИР"), PRODUCTION_AGREEMENT("Производственное соглашение"),
    LICENSE_AGREEMENT("Лицензионное соглашение"), JOINT_VENTURE_AGREEMENT("Соглашение о создании совместного предприятия"),
    FINANCIATION("Привлечение финансирования"), ANOTHER("Другое");

    @Getter
    private String value;
}
