package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.Date;

import static code.entities.tech.IntellectualPropertyRightsTypes.USA_PATENT;

@Entity
@NoArgsConstructor
public class UsaPatent extends CountryPatent {

    @Builder
    @JsonCreator
    public UsaPatent(@JsonProperty("id") Long id,
                     @JsonProperty("technology") TechnologyEntity technology,
                     @JsonProperty("number") String number,
                     @JsonProperty("registrationDate") Date registrationDate) {
        super(id, technology, USA_PATENT, number, registrationDate);
    }
}
