package code.entities.tech;

import code.converters.util.IntellectualRightsUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum  IntellectualPropertyRightsTypes {
    PATENT_APPLICATION("Подана заявка на патент"), RUSSIAN_PATENT("Получен российский патент / свидетельство"),
    EUROPEAN_PATENT("Получен европейский патент"), USA_PATENT("Получен патент США"), ANOTHER("Другое"),
    COMMERCIAL_POSSIBILITIES("Возможное коммерческое использование разработки");

    @Getter
    private String value;
}
