package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.IntellectualPropertyRightsTypes.ANOTHER;

@Entity
@NoArgsConstructor
@Data
public class AnotherRight extends IntellectualPropertyRights{

    private String another;

    @Builder
    @JsonCreator
    public AnotherRight(@JsonProperty("id") Long id,
                        @JsonProperty("technology") TechnologyEntity technology,
                        @JsonProperty("another") String another) {
        super(id, technology, ANOTHER);
        this.another = another;
    }
}
