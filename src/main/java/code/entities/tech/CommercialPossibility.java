package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.IntellectualPropertyRightsTypes.COMMERCIAL_POSSIBILITIES;

@Entity
@NoArgsConstructor
@Data
public class CommercialPossibility extends IntellectualPropertyRights{

    private String branches;

    @Builder
    @JsonCreator
    public CommercialPossibility(@JsonProperty("id") Long id,
                                 @JsonProperty("technology") TechnologyEntity technology,
                                 @JsonProperty("branches") String branches) {
        super(id, technology, COMMERCIAL_POSSIBILITIES);
        this.branches = branches;
    }
}
