package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnerType.ANOTHER;

@Entity
@NoArgsConstructor
@Data
public class AnotherPartner extends Partner{

    private String partner;

    @Builder
    @JsonCreator
    public AnotherPartner(@JsonProperty("id") Long id,
                          @JsonProperty("technology") TechnologyEntity technology,
                          @JsonProperty("partner") String partner) {
        super(id, technology, ANOTHER);
        this.partner = partner;
    }
}
