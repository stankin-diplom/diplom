package code.entities.tech;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "technology_size")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Size {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private Double length, width, height, weight, square;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tech_id")
    private TechnologyEntity technology;
}
