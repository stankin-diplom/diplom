package code.entities.tech;

import code.converters.util.PresentationFormUtil;
import code.converters.util.StageUtil;
import code.converters.util.ThematicUtil;
import code.entities.logistic.LogisticEntity;
import code.entities.membership.MembershipEntity;
import code.entities.organization.OrganizationEntity;
import code.entities.user.UserEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "technologies")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
public class TechnologyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "tech_id")
    private Long id;

    private String ruName;

    private String enName;

    private String exponationGoal;

    private Thematic thematic;

    @ElementCollection
    @CollectionTable(name = "tech_images", joinColumns = @JoinColumn(name = "tech_id"))
    private Set<String> images = new HashSet<>();

    private String developmentGoal;

    // TODO: 27.04.2020 Уточнить формат поля
    private String productionBranch;

    private Stage stage;

    private String marketGeography;

    @Length(max = 1500)
    private String ruDescription;

    @Length(max = 1500)
    private String engDescription;

    private String characteristics;

    private String advantages;

    private Boolean areForeignersInvolved;

    private Double deliveriesVolume;

    @Length(max = 500)
    private String ruShortTechDescription, engShortTechDescription;

    private PresentationForm presentationForm;

    @ElementCollection
    @CollectionTable(name = "publications", joinColumns = @JoinColumn(name = "tech_id"))
    private Set<String> publications = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "technology")
    private Size size;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,CascadeType.REFRESH},mappedBy = "technologies")
    private Set<Author> authors = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "technology")
    private FinancialSources sources;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "technology")
    private IntellectualPropertyRights rights;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "technology")
    private Partnership partnership;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "technology")
    private Partner partner;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "usr_id")
    private Set<UserEntity> users = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "org_id")
    private OrganizationEntity organization;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "memb_id")
    private Set<MembershipEntity> membership = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "technology")
    private LogisticEntity logistic;

    @JsonProperty(value = "stage")
    private void setJsonStage(String stage) {
        this.stage = StageUtil.determineType(stage);
    }

    @JsonProperty(value = "stage")
    private String getJsonStage() {
        if (stage == null)
            return null;

        return stage.getValue();
    }

    @JsonProperty(value = "thematic")
    private void setJsonThematic(String thematic) {
        this.thematic = ThematicUtil.determineType(thematic);
    }

    @JsonProperty(value = "thematic")
    private String getJsonThematic() {
        if (thematic == null)
            return null;

        return thematic.getValue();
    }

    @JsonProperty(value = "presentationForm")
    private void setJsonForm(String presentationForm) {
        this.presentationForm = PresentationFormUtil.determineType(presentationForm);
    }

    @JsonProperty(value = "presentationForm")
    private String getJsonForm() {
        if (presentationForm == null)
            return null;

        return presentationForm.getValue();
    }
}
