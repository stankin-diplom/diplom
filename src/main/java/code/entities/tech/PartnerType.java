package code.entities.tech;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum PartnerType {
    GOVERNMENT_ORGANIZATIONS("Организации правительственного или общественного сектора"),
    EDUCATIONAL_SUBJECTS("Университеты, академии или исследовательские институты "),
    PRODUCTION_ORGANIZATIONS("Промышленные предприятия"),
    BUSINESS("Предприятия среднего и малого бизнеса"),
    PROFESSIONAL_ORGANIZATIONS("Организации, специализирующиеся на профессиональных услугах, консалтинговые компании"),
    ANOTHER("Другое");

    @Getter
    private String value;
}
