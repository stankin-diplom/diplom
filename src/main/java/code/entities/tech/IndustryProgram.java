package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class IndustryProgram extends FulfilledSources{

    @Builder
    @JsonCreator
    public IndustryProgram(@JsonProperty("id") Long id,
                           @JsonProperty("technology") TechnologyEntity technology,
                           @JsonProperty("programName") String programName,
                           @JsonProperty("customer") String customer,
                           @JsonProperty("contractNumber") String contractNumber,
                           @JsonProperty("projectName") String projectName) {
        super(id, "Отраслевая программа", technology, programName, customer, contractNumber, projectName);
    }
}
