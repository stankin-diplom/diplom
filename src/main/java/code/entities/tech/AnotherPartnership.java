package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnershipType.ANOTHER;

@Entity
@NoArgsConstructor
@Data
public class AnotherPartnership extends Partnership{

    private String partnership;

    @Builder
    @JsonCreator
    public AnotherPartnership(@JsonProperty("id") Long id,
                              @JsonProperty("technology") TechnologyEntity technology,
                              @JsonProperty("partnership") String partnership) {
        super(id, technology, ANOTHER);
        this.partnership = partnership;
    }
}
