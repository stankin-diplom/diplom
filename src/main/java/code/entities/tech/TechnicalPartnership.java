package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnershipType.TECHNICAL;

@Entity
@NoArgsConstructor
public class TechnicalPartnership extends Partnership{

    @Builder
    @JsonCreator
    public TechnicalPartnership(@JsonProperty("id") Long id,
                                @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, TECHNICAL);
    }
}
