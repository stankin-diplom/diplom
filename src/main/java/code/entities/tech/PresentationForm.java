package code.entities.tech;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum PresentationForm {
    REAL_SAMPLE("Натуральный образец"), MAKET("Макет"),PROGRAMM_COMPLEX("Программный комплекс"),PRESENTATION("Презентация");

    @Getter
    private String value;
}
