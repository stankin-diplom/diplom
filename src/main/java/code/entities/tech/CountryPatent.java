package code.entities.tech;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.util.Date;

@MappedSuperclass
@Data
@NoArgsConstructor
public class CountryPatent extends IntellectualPropertyRights{

    private String number;

    private Date registrationDate;

    public CountryPatent(Long id,
                         TechnologyEntity technology,
                         @NotNull IntellectualPropertyRightsTypes type,
                         String number,
                         Date registrationDate) {
        super(id, technology, type);
        this.number = number;
        this.registrationDate = registrationDate;
    }
}
