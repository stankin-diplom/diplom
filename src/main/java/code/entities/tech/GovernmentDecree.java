package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class GovernmentDecree extends FulfilledSources{

    @Builder
    @JsonCreator
    public GovernmentDecree(@JsonProperty("id") Long id,
                            @JsonProperty("technology") TechnologyEntity technology,
                            @JsonProperty("programName") String programName,
                            @JsonProperty("customer") String customer,
                            @JsonProperty("contractNumber") String contractNumber,
                            @JsonProperty("projectName") String projectName) {
        super(id, "Постановление Правительства", technology, programName, customer, contractNumber, projectName);
    }
}
