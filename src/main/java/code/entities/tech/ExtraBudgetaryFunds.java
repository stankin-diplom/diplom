package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
public class ExtraBudgetaryFunds extends FinancialSources {

    @Builder
    @JsonCreator
    public ExtraBudgetaryFunds(@JsonProperty("id") Long id,
                               @JsonProperty("technology") TechnologyEntity technology) {
        super(id, "Внебюджетные средства", technology);
    }


}
