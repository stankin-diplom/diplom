package code.entities.tech;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnershipType.PRODUCTION_AGREEMENT;

@NoArgsConstructor
@Entity
public class ProductionAgreement extends Partnership {

    @Builder
    @JsonCreator
    public ProductionAgreement(@JsonProperty("id") Long id,
                               @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, PRODUCTION_AGREEMENT);
    }
}
