package code.entities.tech;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

import static code.entities.tech.PartnerType.PROFESSIONAL_ORGANIZATIONS;

@Entity
@NoArgsConstructor
public class ProfessionalOrganization extends Partner{

    @Builder
    @JsonCreator
    public ProfessionalOrganization(@JsonProperty("id") Long id,
                                    @JsonProperty("technology") TechnologyEntity technology) {
        super(id, technology, PROFESSIONAL_ORGANIZATIONS);
    }
}
