package code.entities.forms;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
public class FieldInformation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "field_info_id")
    private Long id;

    @NotNull
    @NotBlank
    private String sectionName,
    fieldName, type;

    @NotNull
    @NotBlank
    @JsonRawValue
    private String description;

    @NotNull
    private Boolean isCommonInfoRepresentationNeeded,
                    isFullInfoRepresentationNeeded;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reg_form_id")
    private RegistrationFormEntity form;

    @JsonSetter(value = "description")
    private void setJsonDescription(JsonNode jsonDescription){
        this.description = jsonDescription.toString();
    }
}
