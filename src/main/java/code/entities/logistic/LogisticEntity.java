package code.entities.logistic;

import code.entities.dictionaries.Country;
import code.entities.membership.MembershipEntity;
import code.entities.tech.TechnologyEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

@Entity
@Table(name = "logistic")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
public class LogisticEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "logistic_id")
    private Long id;

    private String packaging;

    private Double length;

    private Double height;

    private Double width;

    private Double cubage;

    private Integer amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    private Country country;

    private String TNVEDcode;

    private Double weightNetto;

    private Double weightButto;

    private Double cost;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "logistic")
    private ProductDescription description;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memb_id")
    private MembershipEntity membership;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tech_id")
    private TechnologyEntity technology;
}
