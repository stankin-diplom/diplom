package code.entities.logistic;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product_description")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDescription {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pr_desc_id")
    private Long id;

    private String type;

    private String serialNumber;

    private String mark;

    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "logistic_id")
    private LogisticEntity logistic;
}
