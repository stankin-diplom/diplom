package code.config;

import code.entities.event.EventEntity;
import code.entities.forms.FieldInformation;
import code.entities.forms.RegistrationFormEntity;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RestRepoConfig implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(EventEntity.class,
                RegistrationFormEntity.class,
                FieldInformation.class);
    }
}
