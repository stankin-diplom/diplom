package code.controller.dto;

import lombok.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class TechnologyShortInfoDto {
    private Long id;

    private String image;

    private String name;
}
