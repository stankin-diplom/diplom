package code.controller.dto;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class TechnologyCompleteDto {
    private String name;

    private String description;

    private String developmentGoal;

    private String stage;

    private String areForeignersInvolved;

    private String financiation;

    private List<String> publications;

    private List<String> authors;

    private String logo;
}
