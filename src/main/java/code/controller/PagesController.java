package code.controller;

import code.controller.dto.TechnologyCompleteDto;
import code.controller.dto.TechnologyShortInfoDto;
import code.entities.membership.MembershipEntity;
import code.entities.tech.*;
import code.repositories.event.EventRepository;
import code.repositories.membership.MembershipRestRepository;
import code.repositories.tech.TechnologyRepository;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("pages")
public class PagesController {

    private final EventRepository eventRepository;
    private final TechnologyRepository technologyRepository;
    private final MembershipRestRepository membershipRepository;

    @Autowired
    public PagesController(EventRepository eventRepository,
                           TechnologyRepository technologyRepository,
                           MembershipRestRepository membershipRepository) {
        this.eventRepository = eventRepository;
        this.technologyRepository = technologyRepository;
        this.membershipRepository = membershipRepository;
    }

    @GetMapping("/events")
    public String getEventsPage(Model model) {
        model.addAttribute("events", eventRepository.findAll());

        return "eventsPage";
    }

    @GetMapping("/event/{id}")
    public String getEventTechnologies(@PathVariable("id") Long id, Model model){
        model.addAttribute("thematics", Stream.of(Thematic.values())
                .map(Thematic::getValue)
                .collect(Collectors.toList()
                )
        );

        model.addAttribute("event", eventRepository.findById(id).get());


        return "/thematicsPage";
    }


    @GetMapping("/event/{id}/thematic/{value}")
    public String getTechnologiesPage(@PathVariable("id") Long eventId,
                                      @PathVariable("value") String thematicValue,
                                      Model model) {
        Set<TechnologyShortInfoDto> technologies = membershipRepository.getAllByEvent_Id(eventId).stream()
                .map(MembershipEntity::getTechnologies)
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .map(this::mapToShortInfo)
                .collect(Collectors.toSet());

        model.addAttribute("technologies", technologies);

        return "/technologiesPage";
    }


    @GetMapping("/technology/{id}")
    public String getTechnologyPage(@PathVariable("id") Long id, Model model) {
        technologyRepository.findById(id).ifPresent(entity -> model.addAttribute("technology",
                mapToCompleteDtoInfo(entity)
        ));
        return "/technologyPage";
    }


    private TechnologyShortInfoDto mapToShortInfo(TechnologyEntity technology) {

        TechnologyShortInfoDto infoDto = TechnologyShortInfoDto.builder()
                .name(technology.getRuName())
                .id(technology.getId())
                .build();

        technology.getImages().stream().findFirst().map(FilenameUtils::getName).ifPresent(infoDto::setImage);

        return infoDto;
    }

    private TechnologyCompleteDto mapToCompleteDtoInfo(TechnologyEntity entity) {
        TechnologyCompleteDto dto = TechnologyCompleteDto.builder()
                .authors(Optional.ofNullable(entity.getAuthors())
                        .map(ArrayList::new)
                        .map(list -> ((List<Author>) list))
                        .orElse(Collections.emptyList())
                        .stream()
                        .map(Author::getName)
                        .filter(StringUtils::isNotBlank)
                        .collect(Collectors.toList())
                )
                .name(entity.getRuName())
                .developmentGoal(entity.getDevelopmentGoal())
                .description(entity.getRuDescription())
                .financiation(Optional.ofNullable(entity.getSources())
                        .map(FinancialSources::getType)
                        .orElse("Отсутствует")
                )
                .areForeignersInvolved(entity.getAreForeignersInvolved() ? "Да" : "Нет")
                .stage(Optional.ofNullable(entity.getStage())
                        .map(Stage::getValue)
                        .orElse("Нет данных")
                )
                .publications(Optional.ofNullable(entity.getPublications())
                        .map(ArrayList::new)
                        .map(list -> ((List<String>) list))
                        .orElseGet(Collections::emptyList)
                        .stream()
                        .filter(StringUtils::isNotBlank)
                        .collect(Collectors.toList())
                )
                .build();

        entity.getImages().stream().findFirst().map(FilenameUtils::getName).ifPresent(dto::setLogo);

        return dto;
    }
}
