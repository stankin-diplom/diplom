package code.repositories.catalogue;

import code.entities.catalogue.ExpositionCatalogueEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "catalogues", path = "catalogues")
public interface CatalogueRepository extends CrudRepository<ExpositionCatalogueEntity, Long> {
}
