package code.repositories.membership;

import code.entities.membership.MembershipEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "membership", path = "membership")
public interface MembershipRestRepository extends CrudRepository<MembershipEntity, Long> {
    List<MembershipEntity> getAllByEvent_Id(Long id);
}
