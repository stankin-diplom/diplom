package code.repositories.delegations;

import code.entities.delegation.DelegationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "delegations", path = "delegations")
public interface DelegationsRepository extends CrudRepository<DelegationEntity, Long> {
}
