package code.repositories.delegations;

import code.entities.delegation.Representative;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "representatives", path = "representatives")
public interface RepresentativesRepository extends CrudRepository<Representative, Long> {
}
