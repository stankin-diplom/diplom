package code.repositories.tech.partner;

import code.entities.tech.AnotherPartner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "anotherPartners", path = "anotherPartners")
public interface AnotherPartnerRepository extends CrudRepository<AnotherPartner, Long> {
}
