package code.repositories.tech.partner;

import code.entities.tech.ProductionOrganization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "productionOrganizations", path = "productionOrganizations")
public interface ProductionOrganizationRepository extends CrudRepository<ProductionOrganization, Long> {
}
