package code.repositories.tech.partner;

import code.entities.tech.Partner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "partners", path = "partners")
public interface PartnersRepository extends CrudRepository<Partner, Long> {
}
