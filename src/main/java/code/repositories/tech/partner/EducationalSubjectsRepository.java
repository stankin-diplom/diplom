package code.repositories.tech.partner;

import code.entities.tech.EducationalSubjects;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "educationalSubjects", path = "educationalSubjects")
public interface EducationalSubjectsRepository extends CrudRepository<EducationalSubjects, Long> {
}
