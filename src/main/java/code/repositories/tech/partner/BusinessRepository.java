package code.repositories.tech.partner;

import code.entities.tech.Business;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "business", path = "business")
public interface BusinessRepository extends CrudRepository<Business, Long> {
}
