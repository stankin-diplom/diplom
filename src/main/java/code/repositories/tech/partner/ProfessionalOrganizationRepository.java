package code.repositories.tech.partner;

import code.entities.tech.ProfessionalOrganization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "professionalOrganizations", path = "professionalOrganizations")
public interface ProfessionalOrganizationRepository extends CrudRepository<ProfessionalOrganization, Long> {
}
