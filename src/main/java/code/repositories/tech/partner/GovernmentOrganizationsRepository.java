package code.repositories.tech.partner;

import code.entities.tech.GovernmentOrganizations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "governmentOrganizations", path = "governmentOrganizations")
public interface GovernmentOrganizationsRepository extends CrudRepository<GovernmentOrganizations, Long> {
}
