package code.repositories.tech.sources;

import code.entities.tech.FederalTargetProgram;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "federalTargetPrograms", path = "federalTargetPrograms")
public interface FederalTargetProgramRepository extends CrudRepository<FederalTargetProgram, Long> {
}
