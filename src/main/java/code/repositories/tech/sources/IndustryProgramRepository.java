package code.repositories.tech.sources;

import code.entities.tech.IndustryProgram;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "industryPrograms", path = "industryPrograms")
public interface IndustryProgramRepository extends CrudRepository<IndustryProgram, Long> {
}
