package code.repositories.tech.sources;

import code.entities.tech.GovernmentDecree;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "governmentDecries", path = "governmentDecries")
public interface GovernmentDecreeRepository extends CrudRepository<GovernmentDecree, Long> {
}
