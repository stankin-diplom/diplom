package code.repositories.tech.sources;

import code.entities.tech.AnotherSource;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "anotherSources", path = "anotherSources")
public interface AnotherSourceRepository extends CrudRepository<AnotherSource, Long> {
}
