package code.repositories.tech.sources;

import code.entities.tech.FinancialSources;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sources", path = "sources")
public interface FinancialSourcesRepository extends CrudRepository<FinancialSources, Long> {
}
