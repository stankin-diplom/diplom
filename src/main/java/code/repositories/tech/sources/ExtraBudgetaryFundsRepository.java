package code.repositories.tech.sources;

import code.entities.tech.ExtraBudgetaryFunds;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "extraBudgetaryFunds", path = "extraBudgetaryFunds")
public interface ExtraBudgetaryFundsRepository extends CrudRepository<ExtraBudgetaryFunds, Long> {
}
