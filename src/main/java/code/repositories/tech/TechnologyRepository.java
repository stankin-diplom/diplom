package code.repositories.tech;

import code.entities.membership.MembershipEntity;
import code.entities.tech.TechnologyEntity;
import code.entities.tech.Thematic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Set;

@RepositoryRestResource(collectionResourceRel = "technologies", path = "technologies")
public interface TechnologyRepository extends CrudRepository<TechnologyEntity, Long> {

    Set<TechnologyEntity> findAllByThematicAndMembership(Thematic thematic, MembershipEntity membership);
}
