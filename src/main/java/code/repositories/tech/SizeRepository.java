package code.repositories.tech;

import code.entities.tech.Size;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "size", path = "size")
public interface SizeRepository extends CrudRepository<Size, Long> {
}
