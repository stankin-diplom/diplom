package code.repositories.tech.partnership;


import code.entities.tech.JointVentureAgreement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "jointVentureAgreements", path = "jointVentureAgreements")
public interface JointVentureAgreementRepository extends CrudRepository<JointVentureAgreement, Long> {
}
