package code.repositories.tech.partnership;

import code.entities.tech.NirPartnership;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "nirPartnerships", path = "nirPartnerships")
public interface NirPartnershipRepository extends CrudRepository<NirPartnership, Long> {
}
