package code.repositories.tech.partnership;

import code.entities.tech.AnotherPartnership;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "anotherPartnerships", path = "anotherPartnerships")
public interface AnotherPartnershipRepository extends CrudRepository<AnotherPartnership, Long> {
}
