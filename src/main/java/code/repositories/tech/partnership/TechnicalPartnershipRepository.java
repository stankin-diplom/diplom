package code.repositories.tech.partnership;

import code.entities.tech.TechnicalPartnership;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "technicalPartnerships", path = "technicalPartnerships")
public interface TechnicalPartnershipRepository extends CrudRepository<TechnicalPartnership, Long> {
}
