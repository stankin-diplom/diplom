package code.repositories.tech.partnership;

import code.entities.tech.LicenseAgreement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "licenseAgreements", path = "licenseAgreements")
public interface LicenseAgreementRepository extends CrudRepository<LicenseAgreement, Long> {
}
