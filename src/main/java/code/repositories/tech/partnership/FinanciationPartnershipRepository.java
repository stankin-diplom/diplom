package code.repositories.tech.partnership;

import code.entities.tech.FinanciationPartnership;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "financiationPartnership", path = "financiationPartnership")
public interface FinanciationPartnershipRepository extends CrudRepository<FinanciationPartnership, Long> {
}
