package code.repositories.tech.partnership;

import code.entities.tech.ProductionAgreement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "productionAgreements", path = "productionAgreements")
public interface ProductionAgreementRepository extends CrudRepository<ProductionAgreement, Long> {
}
