package code.repositories.tech.rights;

import code.entities.tech.RussianPatent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "russianPatents", path = "russianPatents")
public interface RussianPatentRepository extends CrudRepository<RussianPatent, Long> {
}
