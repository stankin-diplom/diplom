package code.repositories.tech.rights;

import code.entities.tech.IntellectualPropertyRights;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "rights", path = "rights")
public interface IntellectualPropertyRightsRepository extends CrudRepository<IntellectualPropertyRights, Long> {
}
