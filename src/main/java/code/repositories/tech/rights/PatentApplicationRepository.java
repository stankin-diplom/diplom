package code.repositories.tech.rights;

import code.entities.tech.PatentApplication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "patentApplications", path = "patentApplications")
public interface PatentApplicationRepository extends CrudRepository<PatentApplication, Long> {
}
