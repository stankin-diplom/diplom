package code.repositories.tech.rights;


import code.entities.tech.EuropeanPatent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "europeanPatents", path = "europeanPatents")
public interface EuropeanPatentRepository extends CrudRepository<EuropeanPatent, Long> {
}
