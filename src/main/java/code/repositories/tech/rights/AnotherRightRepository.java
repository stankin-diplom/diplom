package code.repositories.tech.rights;

import code.entities.tech.AnotherRight;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "anotherRights", path = "anotherRights")
public interface AnotherRightRepository extends CrudRepository<AnotherRight, Long> {
}
