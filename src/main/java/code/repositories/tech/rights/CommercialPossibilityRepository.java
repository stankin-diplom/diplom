package code.repositories.tech.rights;

import code.entities.tech.CommercialPossibility;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "commercialPossibilities", path = "commercialPossibilities")
public interface CommercialPossibilityRepository extends CrudRepository<CommercialPossibility, Long> {
}
