package code.repositories.tech.rights;

import code.entities.tech.UsaPatent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "usaPatents", path = "usaPatents")
public interface UsaPatentRepository extends CrudRepository<UsaPatent, Long> {
}
