package code.repositories.forms;

import code.entities.forms.RegistrationFormEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "forms", path = "forms")
public interface RegistrationFormsRepository extends CrudRepository<RegistrationFormEntity, Long> {
}
