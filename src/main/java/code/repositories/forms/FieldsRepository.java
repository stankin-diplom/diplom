package code.repositories.forms;

import code.entities.forms.FieldInformation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "fields", path = "fields")
public interface FieldsRepository extends CrudRepository<FieldInformation, Long> {
}
