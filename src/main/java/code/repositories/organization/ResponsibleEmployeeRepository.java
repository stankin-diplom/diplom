package code.repositories.organization;

import code.entities.organization.ResponsibleEmployee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "employee", path = "employee")
public interface ResponsibleEmployeeRepository extends CrudRepository<ResponsibleEmployee, Long> {
}
