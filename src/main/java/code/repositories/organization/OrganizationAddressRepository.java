package code.repositories.organization;

import code.entities.organization.OrganizationAddress;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "address", path = "address")
public interface OrganizationAddressRepository extends CrudRepository<OrganizationAddress, Long> {
}
