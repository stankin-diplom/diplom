package code.repositories.organization;

import code.entities.organization.OrganizationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "organizations", path = "organizations")
public interface OrganizationRepository extends CrudRepository<OrganizationEntity, Long> {
}
