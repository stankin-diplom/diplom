package code.repositories.event;

import code.entities.event.EventEntity;
import code.entities.tech.TechnologyEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "event", path = "event")
public interface EventRepository extends CrudRepository<EventEntity, Long> {

}
