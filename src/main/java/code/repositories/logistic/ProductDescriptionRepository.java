package code.repositories.logistic;

import code.entities.logistic.ProductDescription;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "description", path = "description")
public interface ProductDescriptionRepository extends CrudRepository<ProductDescription, Long> {
}
