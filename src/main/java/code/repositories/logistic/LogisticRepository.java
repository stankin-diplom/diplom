package code.repositories.logistic;

import code.entities.logistic.LogisticEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "logistic", path = "logistic")
public interface LogisticRepository extends CrudRepository<LogisticEntity, Long> {
}
