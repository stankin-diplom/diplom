package code.repositories.dictionaries;

import code.entities.dictionaries.Subject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "subjects", path = "subjects")
public interface SubjectsRepository extends CrudRepository<Subject, Long> {
}
