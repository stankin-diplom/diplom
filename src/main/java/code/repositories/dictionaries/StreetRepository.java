package code.repositories.dictionaries;

import code.entities.dictionaries.Street;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "streets", path = "streets")
public interface StreetRepository extends CrudRepository<Street, Long> {
}
