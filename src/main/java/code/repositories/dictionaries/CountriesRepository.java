package code.repositories.dictionaries;

import code.entities.dictionaries.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "countries", path = "countries")
public interface CountriesRepository extends CrudRepository<Country, Long> {
}
