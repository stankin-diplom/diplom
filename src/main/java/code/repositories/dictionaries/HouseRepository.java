package code.repositories.dictionaries;

import code.entities.dictionaries.House;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "houses", path = "houses")
public interface HouseRepository extends CrudRepository<House, Long> {
}
