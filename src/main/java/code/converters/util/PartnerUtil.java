package code.converters.util;

import code.entities.tech.PartnerType;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public class PartnerUtil {

    public static PartnerType determineType(String value) {
        return Stream.of(PartnerType.values())
                .filter(type -> StringUtils.equalsIgnoreCase(type.getValue(),value))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
