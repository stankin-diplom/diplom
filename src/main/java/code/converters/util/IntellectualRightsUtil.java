package code.converters.util;

import code.entities.tech.IntellectualPropertyRightsTypes;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public class IntellectualRightsUtil {

    public static IntellectualPropertyRightsTypes determineType(String value) {
        return Stream.of(IntellectualPropertyRightsTypes.values())
                .filter(rightsTypes -> StringUtils.equalsIgnoreCase(rightsTypes.getValue(),value))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
