package code.converters.util;

import code.entities.tech.PartnershipType;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public class PartnershipUtil {

    public static PartnershipType determineType(String value) {
        return Stream.of(PartnershipType.values())
                .filter(type -> StringUtils.equalsIgnoreCase(type.getValue(),value))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
