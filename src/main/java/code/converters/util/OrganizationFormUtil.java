package code.converters.util;

import code.entities.organization.OrganizationForm;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public class OrganizationFormUtil {

    public static OrganizationForm determineType(String value){

        return Stream.of(OrganizationForm.values())
                .filter(form -> StringUtils.equalsIgnoreCase(form.getRussianName(),value))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
