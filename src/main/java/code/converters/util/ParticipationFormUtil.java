package code.converters.util;

import code.entities.organization.ParticipationForm;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public class ParticipationFormUtil {

    public static ParticipationForm determineType(String value){
        return Stream.of(ParticipationForm.values())
                .filter(form -> StringUtils.equalsIgnoreCase(form.getValue(),value))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
