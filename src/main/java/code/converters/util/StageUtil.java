package code.converters.util;

import code.entities.tech.Stage;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public class StageUtil {

    public static Stage determineType(String value) {
        return Stream.of(Stage.values())
                .filter(stage -> StringUtils.equalsIgnoreCase(stage.getValue(),value))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
