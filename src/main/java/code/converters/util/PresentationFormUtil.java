package code.converters.util;

import code.entities.tech.PresentationForm;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public class PresentationFormUtil {

    public static PresentationForm determineType(String value) {
        return Stream.of(PresentationForm.values())
                .filter(form -> StringUtils.equalsIgnoreCase(form.getValue(),value))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
