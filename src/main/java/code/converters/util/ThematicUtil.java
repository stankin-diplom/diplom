package code.converters.util;

import code.entities.tech.Thematic;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public class ThematicUtil {

    public static Thematic determineType(String value) {
        return Stream.of(Thematic.values())
                .filter(thematic -> StringUtils.equalsIgnoreCase(thematic.getValue(),value))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}
