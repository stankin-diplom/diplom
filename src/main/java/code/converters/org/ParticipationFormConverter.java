package code.converters.org;

import code.entities.organization.ParticipationForm;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static code.converters.util.ParticipationFormUtil.determineType;

@Converter(autoApply = true)
public class ParticipationFormConverter implements AttributeConverter<ParticipationForm, String> {

    @Override
    public String convertToDatabaseColumn(ParticipationForm form) {
        if (form == null)
            return null;

        return form.getValue();
    }

    @Override
    public ParticipationForm convertToEntityAttribute(String value) {
        if (StringUtils.isBlank(value))
            return null;

        return determineType(value);
    }
}
