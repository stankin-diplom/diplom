package code.converters.org;

import code.entities.organization.OrganizationForm;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static code.converters.util.OrganizationFormUtil.determineType;

@Converter(autoApply = true)
public class OrganizationFormConverter implements AttributeConverter<OrganizationForm, String> {

    @Override
    public String convertToDatabaseColumn(OrganizationForm form) {
        if (form == null)
            return null;

        return form.getRussianName();
    }

    @Override
    public OrganizationForm convertToEntityAttribute(String value) {
        if (StringUtils.isBlank(value))
            return null;

        return determineType(value);
    }
}
