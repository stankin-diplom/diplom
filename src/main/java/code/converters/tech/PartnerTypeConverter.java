package code.converters.tech;

import code.entities.tech.PartnerType;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static code.converters.util.PartnerUtil.determineType;

@Converter(autoApply = true)
public class PartnerTypeConverter implements AttributeConverter<PartnerType, String> {

    @Override
    public String convertToDatabaseColumn(PartnerType type) {
        if (type == null)
            return null;

        return type.getValue();
    }

    @Override
    public PartnerType convertToEntityAttribute(String value) {
        if (StringUtils.isBlank(value))
            return null;

        return determineType(value);
    }
}
