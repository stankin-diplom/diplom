package code.converters.tech;

import code.entities.tech.Thematic;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static code.converters.util.ThematicUtil.determineType;

@Converter(autoApply = true)
public class ThematicTypeConverter implements AttributeConverter<Thematic, String> {

    @Override
    public String convertToDatabaseColumn(Thematic thematic) {
        if (thematic == null)
            return null;

        return thematic.getValue();
    }

    @Override
    public Thematic convertToEntityAttribute(String value) {
        if (StringUtils.isBlank(value))
            return null;

        return determineType(value);
    }
}
