package code.converters.tech;

import code.entities.tech.Stage;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static code.converters.util.StageUtil.determineType;

@Converter(autoApply = true)
public class StageConverter implements AttributeConverter<Stage, String> {

    @Override
    public String convertToDatabaseColumn(Stage stage) {
        if (stage == null)
            return null;

        return stage.getValue();
    }

    @Override
    public Stage convertToEntityAttribute(String value) {
        if (StringUtils.isBlank(value))
            return null;

        return determineType(value);
    }
}
