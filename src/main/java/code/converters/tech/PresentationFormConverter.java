package code.converters.tech;

import code.entities.tech.PresentationForm;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static code.converters.util.PresentationFormUtil.determineType;

@Converter(autoApply = true)
public class PresentationFormConverter implements AttributeConverter<PresentationForm, String> {

    @Override
    public String convertToDatabaseColumn(PresentationForm form) {
        if (form == null)
            return null;

        return form.getValue();
    }

    @Override
    public PresentationForm convertToEntityAttribute(String value) {
        if (StringUtils.isBlank(value))
            return null;

        return determineType(value);
    }
}
