package code.converters.tech;

import code.entities.tech.PartnershipType;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static code.converters.util.PartnershipUtil.determineType;

@Converter(autoApply = true)
public class PartnershipTypeConverter implements AttributeConverter<PartnershipType, String> {
    @Override
    public String convertToDatabaseColumn(PartnershipType partnershipType) {
        if (partnershipType == null)
            return null;

        return partnershipType.getValue();
    }

    @Override
    public PartnershipType convertToEntityAttribute(String value) {
        if (StringUtils.isBlank(value))
            return null;

        return determineType(value);
    }
}
