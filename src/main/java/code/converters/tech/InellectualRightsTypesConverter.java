package code.converters.tech;

import code.entities.tech.IntellectualPropertyRightsTypes;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static code.converters.util.IntellectualRightsUtil.determineType;

@Converter(autoApply = true)
public class InellectualRightsTypesConverter implements AttributeConverter<IntellectualPropertyRightsTypes, String> {

    @Override
    public String convertToDatabaseColumn(IntellectualPropertyRightsTypes type) {
        if (type == null)
            return null;

        return type.getValue();
    }

    @Override
    public IntellectualPropertyRightsTypes convertToEntityAttribute(String value) {
        if (StringUtils.isBlank(value))
            return null;

        return determineType(value);
    }
}
